Malware Update
===============

After doing some research, I've managed to narrow down what I believe is responsible for my recent malware incident. In my opinion it looks like we are probably dealing with a fork of [Libhijack](https://github.com/SoldierX/Libhijack) given that the code installed on my machine relies on nearly identical dependencies, takes advantage of the same OpenBSD exploit, and runs identical processes.

[Libhijack](https://github.com/SoldierX/Libhijack) is an aggressive remote access trojan for macOS that spreads through Bluetooth and WiFi networks while remaining undetectable to antivirus software.After establishing a connection to a vulnerable system, it overwrites legitimate macOS runtime processes with spyware that can easily obtain root access and steal passwords, files, and anything else it can get its hands on. The developer of Libhijack has an in depth example of how this exploit works at https://www.youtube.com/watch?v=bT_k06Xg-BE.

Unfortunately, most of the source code for Libhijack is gitignored on Github and the processes it runs once installed are aliased and obfuscated in the system logs so I made this repository for educucational purposes.

## Below are some of the files I've found so far that have stood out to me as the most interesting.
### [share/man/man1/ssh](share/man/man1/ssh)
Creates server connection, establishes tunnel

### [share/man/man1/ssh-keygen](share/man/man1/ssh-keygen)
Adds ssh keys, certs to victim machine

### [share/man/man1/ssh-keyscan](share/man/man1/ssh-keygen)
Scans ports and steals public ssh keys for use in known_hosts to avoid detection

### [share/man/man5/ssh_config](share/man/man5/ssh_config)
Gets root access along with other processes it calls within

### [share/man/man5/sshd_config](share/man/man5/sshd_config)
Also helps in getting root access





TODO (ONCE I CAN ACCESS MY FILES)
================================

# Detection
If you are running macOS, check all of the following:

### Check Users & Groups
  * Open System Preferences
  
  * Click Users & Groups
  
  * If you see either of the following, you should shut down your computer and contact someone who can reset your machine immediately:
    
	* A group appearing under the Groups tab that you do not remember adding:
	
		![addedgroup](img/addedgroup.png)
				
	 * A guest user with "Enabled, Managed" credentials:
	 
		![guestuser](img/guestuser.png)

